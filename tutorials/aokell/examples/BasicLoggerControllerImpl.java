/**
 * Dream
 * Copyright (C) 2003-2004 INRIA Rhone-Alpes
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: dream@objectweb.org
 *
 * Initial developer(s): Matthieu Leclercq
 * Contributor(s): Lionel Seinturier
 */

package org.objectweb.fractal.aokell.dream.lib.control.logger;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.objectweb.dream.control.logger.Loggable;
import org.objectweb.dream.control.logger.LoggerControllerRegister;
import org.objectweb.dream.control.logger.Util;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.util.monolog.Monolog;
import org.objectweb.util.monolog.api.BasicLevel;
import org.objectweb.util.monolog.api.Logger;

import org.objectweb.fractal.aokell.lib.control.CloneCtrlException;
import org.objectweb.fractal.aokell.lib.control.Controller;
import org.objectweb.fractal.aokell.lib.membrane.Membrane;

/**
 * Basic implementation of logger controller. A <code>"monolog-conf-file"</code>
 * initialisation parameter can be set to specify the location of the monolog
 * configuration file.
 */
public class BasicLoggerControllerImpl
    implements LoggerControllerItf, Controller
{

  String baseName;

  /** Map associating logger name and list of registred loggables */
  Map    registrations;
  /** Map associating logger name and logger instance */
  Map    loggers;
  Logger baseLogger;
  
  
  public BasicLoggerControllerImpl() {
      initFcController();
  }
  
  private void initFcController() {
      
      String monologConfFile = "etc/monolog.properties";
      if (Monolog.monologFactory == Monolog.getDefaultMonologFactory())
      {
        Monolog.getMonologFactory(monologConfFile);
      }
      
      baseName = Util.getNextUnnamedBaseName();
      registrations = new HashMap();
      loggers = new HashMap();
  }

  // -------------------------------------------------------------------------
  // Fields and methods added and overriden by the mixin class
  // -------------------------------------------------------------------------

  /**
   * @see LoggerControllerItf#getBaseName()
   */
  public String getBaseName()
  {
    return baseName;
  }

  /**
   * @see LoggerControllerItf#setBaseName(String)
   */
  public void setBaseName(String name)
  {
    this.baseName = name;
    baseNameChanged();
  }

  /**
   * @see LoggerControllerItf#getLoggerLevel(String)
   */
  public int getLoggerLevel(String loggerName)
  {
    Logger l = (Logger) loggers.get(loggerName);
    if (l == null)
    {
      if (loggerName == null)
      {
        if (baseLogger == null)
        {
          baseLogger = Monolog.monologFactory.getLogger(baseName);
        }
        l = baseLogger;
      }
      else
      {
        return INHERIT;
      }
    }
    int i = l.getCurrentIntLevel();
    if (i == BasicLevel.DEBUG)
      return DEBUG;
    if (i == BasicLevel.INFO)
      return INFO;
    if (i == BasicLevel.WARN)
      return WARN;
    if (i == BasicLevel.ERROR)
      return ERROR;
    if (i == BasicLevel.FATAL)
      return FATAL;
    if (i == BasicLevel.INHERIT)
      return INHERIT;
    throw new IllegalStateException("Unknown logger level : " + i);
  }

  /**
   * @see LoggerControllerItf#setLoggerLevel(String, int)
   */
  public void setLoggerLevel(String loggerName, int level)
  {
    Logger l = (Logger) loggers.get(loggerName);
    if (l == null)
    {
      if (loggerName == null)
      {
        if (baseLogger == null)
        {
          baseLogger = Monolog.monologFactory.getLogger(baseName);
        }
        l = baseLogger;
      }
      else
      {
        return;
      }
    }
    switch (level)
    {
      case DEBUG :
        l.setLevel(BasicLevel.LEVEL_DEBUG);
        break;
      case INFO :
        l.setLevel(BasicLevel.LEVEL_INFO);
        break;
      case WARN :
        l.setLevel(BasicLevel.LEVEL_WARN);
        break;
      case ERROR :
        l.setLevel(BasicLevel.LEVEL_ERROR);
        break;
      case FATAL :
        l.setLevel(BasicLevel.LEVEL_FATAL);
        break;
      case INHERIT :
        l.setLevel(BasicLevel.LEVEL_INHERIT);
        break;
      default :
        throw new IllegalArgumentException("Unknown level " + level);
    }
  }

  /**
   * @see LoggerControllerItf#getLoggerNames()
   */
  public String[] getLoggerNames()
  {
    return (String[]) loggers.keySet().toArray(new String[loggers.size()]);
  }

  /**
   * @see LoggerControllerRegister#register(String, Loggable)
   */
  public void register(String loggerName, Loggable loggable)
  {
    Set s = (Set) registrations.get(loggerName);
    if (s == null)
    {
      s = new HashSet();
      registrations.put(loggerName, s);
    }
    s.add(loggable);
    giveLogger(loggerName, loggable);
  }

  /**
   * @see LoggerControllerRegister#unregiser(String, Loggable)
   */
  public void unregiser(String loggerName, Loggable loggable)
  {
    Set s = (Set) registrations.get(loggerName);
    if (s == null)
    {
      return;
    }
    s.remove(loggable);
    if (s.isEmpty())
    {
      // no more registration for this logger, remove reference to it, for
      // garbage collector
      registrations.remove(loggerName);
      loggers.remove(loggerName);
    }
  }

  // ---------------------------------------------------------------------------
  // Utility methods
  // ---------------------------------------------------------------------------

  void baseNameChanged()
  {
    loggers.clear();
    Iterator iter = registrations.entrySet().iterator();
    while (iter.hasNext())
    {
      Map.Entry entry = (Map.Entry) iter.next();
      String loggerName = (String) entry.getKey();
      Set loggables = (Set) entry.getValue();
      Iterator iter2 = loggables.iterator();
      while (iter2.hasNext())
      {
        Loggable loggable = (Loggable) iter2.next();
        giveLogger(loggerName, loggable);
      }
    }
  }

  void giveLogger(String loggerName, Loggable loggable)
  {
    Logger logger = (Logger) loggers.get(loggerName);
    if (logger == null)
    {
      String name;
      if (loggerName == null)
      {
        name = baseName;
      }
      else
      {
        name = baseName + "." + loggerName;
      }
      logger = Monolog.monologFactory.getLogger(name);
      loggers.put(loggerName, logger);
    }
    loggable.setLogger(loggerName, logger);
  }

  // --------------------------------------------------------------
  // Controller interface
  // --------------------------------------------------------------
  
  /**
   * Initialize the controller.
   * 
   * @param membrane  the membrane which contains the controller
   */
  public void initFcCtrl( Membrane membrane ) {
      // Indeed nothing
  }
  
  /**
   * Clone the controller state from the current component to another one.
   * This method may receive some hints on how to do this, or provide some
   * hints on how this has been done. For instance, the hints may be a map
   * that is read and/or written by the controller. The raison d'�tre of
   * these hints is that when its state is cloned, a controller may produce
   * results that are needed by other controllers.
   * 
   * @param dst    the destination component
   * @param hints  hints for performing the operation
   */
  public void cloneFcCtrl( Component dst, Object hints )
  throws CloneCtrlException {
      // Indeed nothing for the component controller
  }
}
