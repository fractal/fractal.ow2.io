package org.objectweb.fractal.aokell.dream.lib.control.logger;

import org.objectweb.dream.control.logger.LoggerController;
import org.objectweb.dream.control.logger.LoggerControllerRegister;
import org.objectweb.fractal.aokell.lib.type.InterfaceTypeImpl;
import org.objectweb.fractal.api.type.InterfaceType;

/**
 * Unified interface for LoggerControllerRegister and LoggerController.
 * Both interfaces are implemented by BasicLoggerControllerImpl.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@lifl.fr>
 */
public interface LoggerControllerItf
    extends
        LoggerController,
        LoggerControllerRegister {

    final public static String NAME = "logger-controller";
    
    final public static InterfaceType TYPE =
      new InterfaceTypeImpl(
              NAME,
              LoggerControllerItf.class.getName(),
              false, false, false);

}
